require 'rails_helper'
require 'shared/file_names'

describe ContactsCSVImporter do
  def csv_content
    <<~HEREDOC
      First Name,Last Name,Email Address,Phone Number,Company Name
      Gerhard,Kautzer,gerhardkautzer@cronabayer.com,1-207-643-1816,Hodkiewicz-Lynch
      Myra,Crona,myracrona@schinner.info,(724)196-9470 x998,Champlin-Hahn
      Josh,Donnelly,joshdonnelly@macejkovic.us,081-799-3139 x248,Casper Group
      Kaya,Luettgen,kayaluettgen@christiansen.name,(511)745-9273,"Wyman, Trantow and Hane"
    HEREDOC
  end

  def file(content)
    Tempfile.new.tap { |f| f.write(content) }.tap(&:rewind)
  end

  describe '#perform' do
    subject(:perform) { described_class.new.perform(data) }

    context 'when nil' do
      let(:data) { nil }

      it { is_expected.to be false }
    end

    context 'when 1' do
      let(:data) { 1 }

      it { is_expected.to be false }
    end

    context "when ''" do
      let(:data) { file('') }

      it { is_expected.to be false }
      it { expect { perform }.to change { Contact.count }.by(0) }
    end

    context 'when good csv file with 20 contacts' do
      let(:data) { file_fixture(CONTACTS_CSV_FILE_NAME).open }

      it { is_expected.to be true }
      it { expect { perform }.to change { Contact.count }.by(20) }
    end

    context 'when bad headers' do
      let(:data) { file("First name wrong\nGood name") }

      it { is_expected.to be false }

      it 'does not create a Contact' do
        expect { perform }.to change { Contact.count }.by(0)
      end
    end

    context 'when good headers and bad contacts data' do
      let(:data) { file("First name,Email Address\nGood name,good@email.com\nOk,bad-email") }

      it { is_expected.to be false }

      it 'does not create any contacts' do
        expect { perform }.to change { Contact.count }.by(0)
      end
    end

    context 'when good data' do
      let(:data) { file(csv_content) }

      it { is_expected.to be true }

      it 'creates all Contact' do
        expect { perform }.to change { Contact.count }.by(4)
      end
    end
  end
end
