require 'rails_helper'
require 'shared/file_names'

RSpec.describe Contact, type: :system do
  it 'creates multiple contacts when uploading a csv file and destroys one' do
    visit contacts_path

    attach_file('contacts_csv_file', file_fixture(CONTACTS_CSV_FILE_NAME).realpath)
    click_button 'Upload'

    expect(page).to have_content('gerhardkautzer@cronabayer.com')
    expect(page).to have_content('Kaycee Hauck')
    expect(page).to have_content('Contacts imported!')

    contact_row = find('td', text: 'Kaycee Hauck').ancestor('tr')
    contact_row.click_on 'Destroy'

    expect(page).to have_no_content('Kaycee Hauck')
  end

  it 'filters .com contacts' do
    Contact.create!(email: 'gerhardkautzer@cronabayer.com')
    Contact.create!(email: 'myracrona@schinner.info')

    visit contacts_path

    checkbox = find('label', text: 'Show only ".com" emails').find('input[type=checkbox]')

    expect(page).to have_content('gerhardkautzer@cronabayer.com')
    expect(page).to have_content('myracrona@schinner.info')

    checkbox.click

    expect(page).to have_content('gerhardkautzer@cronabayer.com')
    expect(page).to have_no_content('myracrona@schinner.info')

    checkbox.click

    expect(page).to have_content('gerhardkautzer@cronabayer.com')
    expect(page).to have_content('myracrona@schinner.info')
  end

  it 'order by email' do
    email_column = 2

    Contact.create!(email: 'z@z.z')
    Contact.create!(email: 'a@a.a')

    visit contacts_path

    checkbox = find('label', text: 'Order by email').find('input[type=checkbox]')

    expect(page.all("td:nth-child(#{email_column})").collect(&:text)).to eq(['z@z.z', 'a@a.a'])

    checkbox.click

    expect(page.all("td:nth-child(#{email_column})").collect(&:text)).to eq(['a@a.a', 'z@z.z'])

    checkbox.click

    expect(page.all("td:nth-child(#{email_column})").collect(&:text)).to eq(['z@z.z', 'a@a.a'])
  end
end
