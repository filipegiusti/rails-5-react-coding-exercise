require 'rails_helper'
require 'shared/factories'

RSpec.describe Contact, type: :model do
  include_context 'with factories'

  describe '.create' do
    it 'creates a new contact' do
      expect { contact }.to change { Contact.count }.by(1)
    end
  end
end
