require 'rails_helper'
require 'shared/file_names'
require 'shared/factories'

RSpec.describe ContactsController, type: :controller do
  describe 'GET #index' do
    subject { get :index }

    it { expect(response).to be_success }
  end

  describe 'PATCH #bulk_create' do
    subject(:perform) { patch :bulk_create, params: params }

    context 'with valid csv file' do
      let(:params) do
        { contacts_csv_file: fixture_file_upload(file_fixture(CONTACTS_CSV_FILE_NAME)) }
      end

      it { expect { perform }.to change { Contact.count }.by(20) }

      it { is_expected.to redirect_to(contacts_path) }

      it 'sets flash.notice' do
        perform
        expect(flash.notice).to eq('Contacts imported!')
      end
    end

    context 'without csv file' do
      let(:params) { {} }

      it { is_expected.to redirect_to(contacts_path) }

      it 'sets flash.notice' do
        perform
        expect(flash.notice).to eq('Failure to import contacts.')
      end
    end
  end

  describe 'DELETE #destroy with json' do
    include_context 'with factories'

    subject(:perform) do
      request.headers['Accept'] = 'application/json'
      delete :destroy, params: { id: contact.to_param }
    end

    before { contact }

    it { expect { perform }.to change { Contact.count }.by(-1) }

    it { is_expected.to have_http_status(:no_content) }
  end
end
