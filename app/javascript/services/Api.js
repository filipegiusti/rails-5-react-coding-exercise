/* global global */
import axios from "axios"

function csrfToken () {
  var meta;
  meta = global.document.querySelector("meta[name=csrf-token]");
  return meta && meta.content;
}

axios.defaults.headers.common["X-CSRF-Token"] = csrfToken();
axios.defaults.headers.common["Accept"] = "application/json";

class Api {
  constructor(path) {
    this.path = path;
  }

  destroy(id, destroySuccess, requestFailure, requestError) {
    return axios.delete(`${this.path}/${id}`)
           .then(() => destroySuccess(id))
           .catch((error) => {
             if (error.response) {
               // 404 on destroying means the object doesn't exists, thus a success
               if (error.response.status == 404) {
                 destroySuccess(id);
               } else {
                 requestFailure(error.response, id);
               }
             } else if (error.request) {
               requestError(error.request, id);
             } else {
               throw error.message;
             }
           })
  }
}

export default Api
