import React from "react"
import Contacts from "components/Contacts"
import Checkbox from "components/Checkbox"
import Api from "services/Api"

class ContactsController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: props.initialItems.map(item => Object.assign(item, { visible: true })),
      toOrder: null,
    };
    this.api = new Api("/contacts");
  }

  removeItem (id) {
    this.setState(prevState => ({
      items: prevState.items.filter(item => ( item.id != id ))
    }))
  }

  updateItemsState(toUpdateItem) {
    this.setState(prevState => ({
      items: prevState.items.map(item => Object.assign(item, toUpdateItem(item)))
    }))
  }

  setVisibility(id, visible) {
    this.updateItemsState(item => (
      item.id == id ? { visible: visible } : {}
    ))
  }

  notice (message) {
    // TODO: Show notice for user
    console.log(`NOTICE: ${message}`); // eslint-disable-line
  }

  destroySuccess = (id) => {
    this.removeItem(id);
    this.notice("Contact was successfully destroyed.");
  }

  requestFailure = (response, id) => {
    this.notice("Internal server error.")
    // TODO: Track this errors
    console.log(`Destroy response code: ${response.status}`); // eslint-disable-line
    this.setVisibility(id, true);
  }

  requestError = (request, id) => {
    this.notice("Connection error.");
    this.setVisibility(id, true);
  }

  onDestroy = (id) => {
    this.setVisibility(id, false);
    this.api.destroy(id, this.destroySuccess, this.requestFailure, this.requestError)
  }

  filterEmails = () => {
    this.updateItemsState(item => ({ visible: item.email.endsWith(".com") }));
  }

  unfilterEmails = () => {
    this.updateItemsState(() => ({ visible: true }));
  }

  orderByEmail (a, b) {
    return a.email.localeCompare(b.email);
  }

  setOrderByEmail = () => {
    this.setState({toOrder: this.orderByEmail});
  }

  unsetOrder = () => {
    this.setState({toOrder: null});
  }

  get visibleItems() {
    var items = this.state.items.filter(item => item.visible);
    return typeof this.state.toOrder == "function" ? items.sort(this.state.toOrder) : items
  }

  render () {
    return (
      <React.Fragment>
        <Checkbox
          label='Show only ".com" emails'
          onCheck={this.filterEmails}
          onUncheck={this.unfilterEmails}
        />
        <Checkbox
          label='Order by email'
          onCheck={this.setOrderByEmail}
          onUncheck={this.unsetOrder}
        />
        <Contacts onDestroy={this.onDestroy} items={this.visibleItems}/>
      </React.Fragment>
    );
  }
}

ContactsController.propTypes = {
  initialItems: Contacts.propTypes.items,
};

export default ContactsController
