import React from "react"
import PropTypes from "prop-types"

class Checkbox extends React.Component {
  handleChange = (e) => {
    if (e.target.checked) {
      this.props.onCheck();
    } else {
      this.props.onUncheck();
    }
  }

  render () {
    return (
      <label>
        <input type="checkbox" onChange={this.handleChange} />
        {this.props.label}
      </label>
    )
  }
}

Checkbox.propTypes = {
  label: PropTypes.string,
  onCheck: PropTypes.func.isRequired,
  onUncheck: PropTypes.func.isRequired,
};

export default Checkbox
