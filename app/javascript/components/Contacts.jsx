import React from "react"
import PropTypes from "prop-types"
import Contact from './Contact'

class Contacts extends React.Component {
  render () {
    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Company</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {this.props.items.map((item) =>
            <Contact key={item.id} onDestroy={this.props.onDestroy} {...item}/>
          )}
        </tbody>
      </table>
    );
  }
}

Contacts.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape(Contact.propTypes)).isRequired,
  onDestroy: PropTypes.func.isRequired,
};

export default Contacts
