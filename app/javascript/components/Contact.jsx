import React from 'react'
import PropTypes from 'prop-types'

class Contact extends React.Component {
  get name () {
    return `${this.props.firstName} ${this.props.lastName}`;
  }

  handleDestroyClick = (e) => {
    e.preventDefault();
    this.props.onDestroy(this.props.id);
  }

  render () {
    return (
      <tr>
        <td>{this.name}</td>
        <td>{this.props.email}</td>
        <td>{this.props.phone}</td>
        <td>{this.props.company}</td>
        <td>
          <a onClick={this.handleDestroyClick} rel="nofollow" href="#">
            Destroy
          </a>
        </td>
      </tr>
    );
  }
}

Contact.propTypes = {
  id: PropTypes.number,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  company: PropTypes.string,
  onDestroy: PropTypes.func,
};

export default Contact
