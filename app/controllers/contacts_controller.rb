class ContactsController < ApplicationController
  def index
    @contacts = Contact.all
  end

  def bulk_create
    result = ContactsCSVImporter.new.perform(permitted_params.fetch(:contacts_csv_file, nil))

    feedback_message = result ? 'Contacts imported!' : 'Failure to import contacts.'

    redirect_to contacts_path, notice: feedback_message
  end

  def destroy
    find_contact.destroy

    respond_to :json
  end

  private

  def permitted_params
    params.permit(:contacts_csv_file)
  end

  def find_contact
    Contact.find(params[:id])
  end
end
