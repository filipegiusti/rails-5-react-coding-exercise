require 'csv'

class ContactsCSVImporter
  ALLOWED_HEADERS =
    ['First Name', 'Last Name', 'Email Address', 'Phone Number', 'Company Name'].freeze
  MAP_HEADERS = {
    'email_address' => 'email',
    'phone_number' => 'phone',
    'company_name' => 'company',
  }.freeze

  def perform(data)
    return false if data.nil? || !data.respond_to?(:to_io)

    headers, contacts_params = parse(data.to_io)

    return false if contacts_params.empty?
    return false unless valid_headers?(headers)

    create_contacts(contacts_params)
  end

  private

  def create_contacts(contacts_params)
    Contact.transaction do
      contacts_params.each { |params| Contact.find_or_create_by!(params) }
    end

    true
  rescue ActiveRecord::RecordInvalid
    false
  end

  def parse(string_or_io)
    parse_options = {
      headers: true,
      header_converters: normalize_headers,
      return_headers: true,
    }

    csv = CSV.new(string_or_io, parse_options)
    headers = csv.find(&:header_row?)&.headers || []
    contacts_params = csv.reject(&:header_row?).map(&:to_hash)

    [headers, contacts_params]
  end

  def valid_headers?(headers)
    normalized_allowed_headers = ALLOWED_HEADERS.map(&normalize_headers)
    (headers - normalized_allowed_headers).empty?
  end

  def normalize_headers
    # "Email Address" to "email" from MAP_HEADERS
    # "First Name" to "first_name"
    lambda do |header|
      converted_header = header.tr(' ', '_').downcase
      MAP_HEADERS.fetch(converted_header, converted_header)
    end
  end
end
