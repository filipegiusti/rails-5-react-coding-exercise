require 'mail'

class EmailValidator < ActiveModel::EachValidator
  class MissingDomainError < StandardError; end

  def validate_each(record, attribute, value)
    Mail::Address.new(value).domain.present? || raise(MissingDomainError)
  rescue Mail::Field::ParseError, MissingDomainError
    record.errors[attribute] << (options[:message] || 'is not an email')
  end
end
