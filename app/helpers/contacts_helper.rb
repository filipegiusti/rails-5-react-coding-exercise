module ContactsHelper
  def render_contacts_for_react(contacts)
    json = ActiveModel::Serializer::CollectionSerializer.new(contacts).as_json

    react_component('ContactsController', { initial_items: json }, camelize_props: true)
  end
end
