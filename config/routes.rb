Rails.application.routes.draw do
  root to: 'contacts#index', as: :contacts

  resources :contacts, only: %i[destroy] do
    collection do
      patch '', action: 'bulk_create', as: 'bulk_create'
    end
  end
end
